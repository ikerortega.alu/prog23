
import java.sql.*;

public class ex13bResultSetInsertRow {

         public static void main(String[] args) {
        try ( Connection connexio = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/empresa","root","root");
        	Statement stmt = connexio.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                      ResultSet.CONCUR_UPDATABLE);
        	ResultSet rs = stmt.executeQuery("select * from clientes");	
        ) {
            // Modifica l'adreça de l'últim client
            System.out.println("Situem el cursor al final");
            System.out.println("Modifiquem l'adreça de l'últim registre");
            rs.last();
            rs.updateString("direccion", "C/ Pepe Ciges, 3");
            rs.updateRow();
            
            //Creem un nou registre en la taula de clients
            System.out.println("Creem un nou client");
            rs.moveToInsertRow();
            rs.updateString(2,"Killy Lopez");
            rs.updateString(3,"Wall Street 3674");
            rs.insertRow();

            // Ens situem en el segon client del ResultSet
            System.out.println("Esborrem el segon registre");
            rs.absolute(2);
            rs.deleteRow();
 
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        }  
    }
    
}
