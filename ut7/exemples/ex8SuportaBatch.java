
import java.sql.*;

public class ex8SuportaBatch {

        public static void main(String[] args) {

        try ( Connection conn = DriverManager.getConnection("jdbc:mysql://172.17.0.3:3306/empresa","root","root");
        	Statement stmt = conn.createStatement();)
        { 
            //COMPROVAR SUPORT BATCH
            DatabaseMetaData dm = conn.getMetaData();
            System.out.println("Suporta processament batch -> "+dm.supportsBatchUpdates());
            
        } catch(SQLException se) {
            //Errors de JDBC
            se.printStackTrace();
        }
    }
    
}
