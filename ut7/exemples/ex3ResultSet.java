// FER ÚS DE CLASSPATH (-cp) AL COMPILAR I EXECUTAR, AMB LA RUTA AL JAR DE MYSQL


import java.sql.*;

public class ex3ResultSet {
    
    public static void main(String[] args) {

        try ( Connection connexio = DriverManager.getConnection("jdbc:mysql://172.17.0.3:3306/empresa","root","root");
        	// createStatement() SENSE PARÀMETRES condiciona el tipus de Statement obtingut
        	/* Equivalent a
        	Statement stmt = connexio.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY); */
        	Statement stmt = connexio.createStatement();
        	ResultSet rs = stmt.executeQuery("select * from grupos");	
        ) {

            // ÚS DEL MÈTODE executeQuery
            

/*            System.out.println("Timeout de consulta: "+stmt.getQueryTimeout()+ " sg");
            System.out.println("Nombre màxim de registres a buscar: "+stmt.getMaxRows());
            System.out.println("Mida màxima de camp: "+stmt.getMaxFieldSize()+" bytes");
            System.out.println("Nombre de registros retornats cada vegada: "+stmt.getFetchSize());*/
            int id ; String descr;
            while ( rs.next() )	// AMB AQUEST STATEMENT PUC AVANÇAR PERÒ NO PUC RETROCEDIR 
            { 
            	System.out.println("Canviant a una nova fila");
            	//id = rs.getInt(1);	// llig la primera columna (1) de la fila actual, que és int (getInt)
            	id = rs.getInt("id");	//, equivalent a la línia anterior
            	//descr = rs.getString(2);
            	descr = rs.getString("descripcion");	// equivalent a la línia anterior
            	System.out.println("Identificador: " + id + ", descripció: " + descr);
            }

        } catch(SQLException se) {
            se.printStackTrace();
        }
    }
    
}
