/* Exemple de TRANSACCIÓ (TOT  O RES)
 */
import java.sql.*;
public class ex7TransaccioDiscos {

        public static void main(String[] args) {
        try ( Connection conn = DriverManager.getConnection("jdbc:mysql://172.17.0.3:3306/musica","root","root");) 
	{
		try (Statement stmt = conn.createStatement();)
		{
		    // INICI DE LA TRANSACCIÓ
		    conn.setAutoCommit(false);
		    String sql = "insert into musics VALUES (4,'Duke Ellington','jazz')";
		    stmt.executeUpdate(sql);
			sql = "select count(*) from discos where id=6";
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int res = rs.getInt(1);
			if (res > 0)
				throw new SQLException("Eixa fila ja existeix");
		    sql = "insert into discos VALUES (6,'Take the A train',15,4)";
		    stmt.executeUpdate(sql);
		    // explícitament actualitze la base de dades amb commit()
		    conn.commit();
		    conn.setAutoCommit(true);
		}
		catch(SQLException se) {
			conn.rollback();
			conn.setAutoCommit(true);
			throw se;
		} 
	} 
	catch(SQLException e) {
		System.out.println(e.getMessage());
		} 
	}
}
