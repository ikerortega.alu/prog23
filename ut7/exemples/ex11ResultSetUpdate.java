/* Exemple de SELECT utilitzant RESULTSET
 */
import java.sql.*;

public class ex11ResultSetUpdate {
    
    public static void main(String[] args) {
        try ( Connection conn = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/musica","root","root");
        	//Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
        	Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                      ResultSet.CONCUR_UPDATABLE);
        	ResultSet rs = stmt.executeQuery("select * from discos");	
        ) {
            int cod,nMusic; String titol; double preu; 
            while (rs.next())
            {
            	cod = rs.getInt(1);
		if (cod ==5)
		{
			// Requereix 2 updates: actualitzar primer la columna i després la fila sencera
			//rs.updateDouble(4,25);
			rs.updateDouble("preu",25);
			rs.updateRow();
		}
            	titol = rs.getString(2);
            	preu = rs.getDouble("preu");
            	nMusic = rs.getInt("music");
            	System.out.println("Id: " + cod + ",\t" + titol + ", preu: " + preu + " euros, del music " + nMusic);
            }
            
        } catch(SQLException se) {
            //Errors de JDBC
            se.printStackTrace();
        }
    }
}
