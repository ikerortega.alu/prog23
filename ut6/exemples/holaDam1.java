import javax.swing.*;

public class holaDam1
{
	public static void main(String args[])
	{
		JFrame jf = new JFrame("Primer programa amb Swing");
		//jf.setSize(640,480);
		
		jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		// per a centrar el JFrame en pantalla
		jf.setLocationRelativeTo(null);
		JLabel jl = new JLabel("Hola DAM1!!",SwingConstants.RIGHT);
		// he d'afegir el JLabel al JFrame
		jf.getContentPane().add(jl);
		// dimensiona el JFrame en funció del seu contingut, SEMPRE DESPRÉS D'AFEGIR ELS COMPONENTS
		jf.pack();
		jf.setVisible(true);
	}
}
