import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class exSlider {
	static final int MIN = 0;
	static final int MAX = 1000;
	static final int INIT = 500;	// valor inicial
	private static JLabel label = new JLabel("1 Euro són en dolars:");
	private static JLabel lbleuros = new JLabel("Euros:");
	private static JLabel lbldolars = new JLabel("dolars:");
	private static JFrame frame = new JFrame("Conversor Euros - dolars");
	private static JPanel panell1 = new JPanel();
	private static JPanel panell2 = new JPanel();
	private static JPanel panell3 = new JPanel();
	private static JTextField txteuro = new JTextField("0");
	private static JTextField txtdolar = new JTextField("0");
	private static JTextField txtcanvi = new JTextField("1.36");
	private static JSlider sliderdolar = new JSlider(JSlider.HORIZONTAL,MIN,MAX,INIT);
	private static JSlider slidereuro = new JSlider(JSlider.HORIZONTAL,MIN,MAX,INIT);
	
	// respondrà a events de canvi als requadres de text
	public static void canvitexto(ActionEvent e) {
		// Comprove quin requadre de text ha canviat
		if ( e.getSource() == txteuro ){
			// llig el seu nou valor
			float icanvi=Float.parseFloat(txteuro.getText());
			// faig conversió segons el canvi i multiplicant per 100 per a no perdre precisió
			icanvi=100*icanvi*Float.parseFloat(txtcanvi.getText());
			// arredoniment
			icanvi = Math.round(icanvi);
			// i el torne a dividir per 100 per a deixar-lo en el seu valor correcte
			icanvi = icanvi/100;
	// canvie l'altre requadre de text per a actualizar-lo al valor obtingut en l'anterior càlcul
            txtdolar.setText(String.valueOf(icanvi));
            // canvie els slider
            sliderdolar.setValue(Math.round(Float.parseFloat(txtdolar.getText())));
            slidereuro.setValue(Math.round(Float.parseFloat(txteuro.getText())));
		}
		if ( e.getSource() == txtdolar ){
			System.out.println("dentro");
			float icanvi=Float.parseFloat(txtdolar.getText());
			icanvi = 100*icanvi/Float.parseFloat(txtcanvi.getText());
			icanvi = Math.round(icanvi);
			icanvi = icanvi/100;
            txteuro.setText(String.valueOf(icanvi));
            // falta codi
                        sliderdolar.setValue(Math.round(Float.parseFloat(txtdolar.getText())));
            		slidereuro.setValue(Math.round(Float.parseFloat(txteuro.getText())));
		}
	}

	// respondrpa als esdeveniments de desplaçament dels JSlider
	public static void mueveSlider(ChangeEvent e) {
		int valor;
		JSlider obj = (JSlider) e.getSource();
		System.out.println(obj.getValueIsAdjusting());
		System.out.println(obj.getValue());
		// quan s'acabe de desplatzar el JSlider
		if (!obj.getValueIsAdjusting()) {
			System.out.println(obj.getValue());
			valor = (int)obj.getValue();
			if (obj == sliderdolar){
				txtdolar.setText(String.valueOf(valor));
				float icanvi=100*valor/Float.parseFloat(txtcanvi.getText());
				icanvi=Math.round(icanvi);
				icanvi=icanvi/100;
				//canviar el txteuro
				txteuro.setText(String.valueOf(icanvi));
				//canviar el slidereuro
				int i = Math.round(icanvi);
				slidereuro.setValue(i);
			}
			if (obj == slidereuro){
				txteuro.setText(String.valueOf(valor));
				float icanvi=100*valor*Float.parseFloat(txtcanvi.getText());
				icanvi=Math.round(icanvi);
				icanvi=icanvi/100;
				//canviar el txtdolar
				txtdolar.setText(String.valueOf(icanvi));
				//canviar el sliderdolar
				int i = Math.round(icanvi);
				sliderdolar.setValue(i);
			}
		}
	}

	public static void colocaelementos()
	{
		frame.getContentPane().add(panell1);
		frame.getContentPane().add(panell2);
		frame.getContentPane().add(panell3);
		
		slidereuro.setBorder(BorderFactory.createTitledBorder("Euros"));
		slidereuro.setMajorTickSpacing(200);
		slidereuro.setMinorTickSpacing(100);
		// posem les marques majors i menors
		slidereuro.setPaintTicks(true);
		// mostrem els valors d'eixes marques
		slidereuro.setPaintLabels(true);
		//slidereuro.disable();

		sliderdolar.setBorder(BorderFactory.createTitledBorder("dolars"));
		sliderdolar.setMajorTickSpacing(200);
		sliderdolar.setMinorTickSpacing(100);
		sliderdolar.setPaintTicks(true);
		sliderdolar.setPaintLabels(true);
		//sliderdolar.disable();	// DEPRECATED

		// incorpore components al panell izquierdo
		panell1.add(lbleuros);
		panell1.add(txteuro);
		panell1.add(slidereuro);
		// incorpore components al panell central
		panell2.add(label);
		panell2.add(txtcanvi);
		// incorpore components al panell derecho
		panell3.add(lbldolars);
		panell3.add(txtdolar);
		panell3.add(sliderdolar);
		frame.addWindowListener(new WindowAdapter() {
			    			public void windowClosing(WindowEvent e) {
							System.exit(0);
			    			}
					});
		txteuro.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
						canvitexto(e);
			    }
		});
		txtdolar.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
						canvitexto(e);
			    }
		});
		
		// afegir Listeners als Sliders
		slidereuro.addChangeListener(new ChangeListener() 
		{	// classe anònima
			public void stateChanged​(ChangeEvent e)
			{
				mueveSlider(e);
			}
		});
		
		sliderdolar.addChangeListener(new ChangeListener() 
		{	// classe anònima
			public void stateChanged​(ChangeEvent e)
			{
				mueveSlider(e);
			}
		});

		frame.setLayout(new FlowLayout());
		panell1.setLayout(new GridLayout(0,1));
		panell2.setLayout(new GridLayout(0,1));
		panell3.setLayout(new GridLayout(0,1));
		frame.pack();
		frame.setVisible(true);

    }
    public static void main(String[] args) {
       /*try {
            UIManager.setLookAndFeel(
                UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) { }*/
        colocaelementos();
    }
}

