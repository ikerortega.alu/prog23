import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

/**
 * Classe que permet carregar una finestra amb un àrea de text
 * i les opcions d'obrir o guardar un fitxer
 *
 */
class FileChooser extends JFrame implements ActionListener
	{
		private Container contenidor;
		JLabel labelTitulo;/*declarem l'objecte Label*/
		JTextArea areaDetext;
		JButton botoObrir;/*declarem l'objecte botó*/
		JButton botoGuardar;/*declarem l'objecte botó*/
		JScrollPane scrollPaneArea;
		JFileChooser fileChooser; /*declarem l'objecte fileChooser*/
		String text;
    
		
		public FileChooser()//constructor
		{
			contenidor=getContentPane();
			contenidor.setLayout(null);
			
			/*Creem l'objecte*/
			fileChooser=new JFileChooser();
			
			/*Propiedades del Label, lo instanciamos, posicionamos y
			 * activem els events*/
			labelTitulo= new JLabel();
			labelTitulo.setText("COMPONENT JFILECHOOSER");
			labelTitulo.setBounds(110, 20, 180, 23);
			
			areaDetext = new JTextArea();
			//per a que el text s'ajuste a l'àrea
			areaDetext.setLineWrap(true);
			//permet que no queden paraules incompletes al fer el salt de línia
			areaDetext.setWrapStyleWord(true);
		   	scrollPaneArea = new JScrollPane();
			scrollPaneArea.setBounds(20, 50, 350, 270);
	        scrollPaneArea.setViewportView(areaDetext);
	       	
			/*Propietats del botó, l'instanciem, el seleccionem i
			 * activem els events*/
			botoObrir= new JButton();
			botoObrir.setText("Obrir");
			botoObrir.setBounds(100, 330, 80, 23);
			botoObrir.addActionListener(this);
			
			botoGuardar= new JButton();
			botoGuardar.setText("Guardar");
			botoGuardar.setBounds(220, 330, 80, 23);
			botoGuardar.addActionListener(this);
			
			/*Agreguem els components al Contenidor*/
			contenidor.add(labelTitulo);
			contenidor.add(scrollPaneArea);
			contenidor.add(botoObrir);
			contenidor.add(botoGuardar);
       		//Assigna un titol a la barra
			setTitle("CoDejaVu : Finestra JFileChooser");
			setSize(400,400);
			//posa la finestra al centre de la pantalla
			setLocationRelativeTo(null);
			
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource()==botoObrir)
			{
				String arxiu=Obrirarxiu();
				areaDetext.setText(arxiu);
			}
			
			if (event.getSource()==botoGuardar)
			{
				guardararxiu();
			}
		}

		/**
		 * Permet mostrar la finestra que carrega
		 * arxius en l'àrea de text	 */
		private String Obrirarxiu() {
					
			String aux=""; 		
	 		text="";
		
	 		try
			{
	 			/*cridem al mètode que permet carregar la finestra*/
	    		fileChooser.showOpenDialog(this);
	    		/*obrim l'arxiu seleccionat*/
	 			File abre=fileChooser.getSelectedFile();

	 			/*recorrem l'arxiu, el llegim per a reflectir-lo
	 			 *en l'àrea de text*/
	 			if(abre!=null)
	 			{ 				
	 				FileReader arxius=new FileReader(abre);
	 				BufferedReader lee=new BufferedReader(arxius);
	 				while((aux=lee.readLine())!=null)
	 					{
	 					 text+= aux+ "\n";
	 					}

	 		  		lee.close();
	 			} 			
	 		}
	 		catch(IOException ex)
			{
			  JOptionPane.showMessageDialog(null,ex+"" +
			  		"\nNo s'ha trobat l'arxiu",
			  		"ADVERTENCIA!!!",JOptionPane.WARNING_MESSAGE);
			}
				return text;
		}
		
		/**
		 * Guardem l'arxiu de l'àrea 
		 * de text
		 */
		private void guardararxiu() {

	 		try
	 		{
				String nom="";
				JFileChooser file=new JFileChooser();
				file.showSaveDialog(this);
				File guarda =file.getSelectedFile();
		
				if(guarda !=null)
				{
		 			nom=file.getSelectedFile().getName();
		 			/*guardem l'arxiu i li donem el format directament*/
		 			FileWriter  save=new FileWriter(guarda+".txt");
		 			save.write(areaDetext.getText());
		 			save.close();
		 			JOptionPane.showMessageDialog(null,
		 					"L'arxiu s'ha guardat correctament",
		 					"Informació",JOptionPane.INFORMATION_MESSAGE);
			    }
	 		 }
	 	   catch(IOException ex)
		   {
			 JOptionPane.showMessageDialog(null,
					 "L'arxiu no s'ha guardat",
					 "Advertència",JOptionPane.WARNING_MESSAGE);
		   }
		}
	}
	
public class exFileChooser
{
	public static void main(String[] args) {
		FileChooser fc = new FileChooser();
		fc.setVisible(true);
	}
}
