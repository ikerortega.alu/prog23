import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class menu extends JFrame implements ActionListener{
    private JMenuBar mb;
    private JMenu menu1;
    private JMenuItem mi1,mi2,mi3;
    public menu() {
        setLayout(null);
        mb=new JMenuBar();
        setJMenuBar(mb);
        menu1=new JMenu("Opcions");
        mb.add(menu1);
        mi1=new JMenuItem("Roig");
        mi1.addActionListener(this);
        menu1.add(mi1);
        mi2=new JMenuItem("Verd");
        mi2.addActionListener(this);
        menu1.add(mi2);
        mi3=new JMenuItem("Blau");
        mi3.addActionListener(this);
        menu1.add(mi3);               
    }
    
    public void actionPerformed(ActionEvent e) {
    	Container f=this.getContentPane();
        if (e.getSource()==mi1) {
            f.setBackground(new Color(255,0,0));
        }
        if (e.getSource()==mi2) {
            f.setBackground(new Color(0,255,0));
        }
        if (e.getSource()==mi3) {
            f.setBackground(new Color(0,0,255));
        }        
    }
    
    public static void main(String[] ar) {
        menu formulari1=new menu();
        formulari1.setBounds(10,20,300,200);
        formulari1.setVisible(true);
        formulari1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }    
}
