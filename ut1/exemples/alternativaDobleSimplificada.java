/* Exemple d'alternativa doble simplificada (d'ús en assignacions)

    Programa que demane 2 valors numèrics i mostre el major */

public class alternativaDobleSimplificada
{
    public static void main(String args[])
    {
        double n1,n2,max;

        System.out.println("Introduix un valor:");
        n1 = Double.parseDouble(System.console().readLine());
        System.out.println("Introduix altre valor:");
        n2 = Double.parseDouble(System.console().readLine());
/*        if (n1 > n2)
            max = n1;
        else
            max = n2;*/
        max = (n1 > n2)? n1 : n2 ;
        System.out.println("El major és " + max);
    }
}
