//12*. Programa que demana un nombre més gran que 0 a l'usuari i realitza comprovació de valor de manera que si no és correcte torne a sol·licitar la introducció del valor.

public class e12
{
    public static void main(String args[])
   {
        double num;        

        System.out.println("Introduce un valor numérico mayor que 0");
        num= Double.parseDouble(System.console().readLine());
        
        while (num <= 0)
            {
                System.out.println("Introduce otra valor numérico");
                num= Double.parseDouble(System.console().readLine());
            }
    }
}
