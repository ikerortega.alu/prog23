// 16. Programa que indique el màxim i el mínim d'un conjunt de notes introduïdes des de teclat. La sèrie de notes acabarà amb un valor negatiu.

public class e16
{
    public static void main(String args[])
    {
        double max=-1,min=11,nota;
        
                System.out.print("Introduce  notas entre 0 y 10(Numero negativo para finalizar): ");
            nota=Double.parseDouble(System.console().readLine());
       while(nota>=0)
        {
            if (nota > max)
                max = nota;
            if (nota < min)
                min = nota;
 //           System.out.print("Introduce una nota entre 0 y 10(Numero negativo para finalizar): ");
            nota=Double.parseDouble(System.console().readLine());
        }
        if (max >= 0)
        {
            System.out.println("Nota maxima: "+max);
            System.out.println("Nota minima: "+min);
        }
        else
            System.out.println("No han hagut notes vàlides");
    }
}
