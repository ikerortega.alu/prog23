/*Algorisme que sol·licita notes introduïdes per teclat acabades
amb un nombre negatiu, i imprimeix en pantalla l'aprovat de nota més baixa i el suspens de nota més alta.*/
public class ex22
{
	public static void main(String args[])
	{
		double nota, min, max;

		System.out.println("Introdueix les notas (negatiu per a acabar): ");
		nota = Double.parseDouble(System.console().readLine());
		min = 11;
		max = -1;

		while(true)
		{
			//si se introduce negativo acaba
			if (nota < 0 )
				break;
			else
				//notas suspendidas
				if (nota < 5)
					max = nota > max? nota : max; //cuando una suspendida es mayor que la anterior se cambian
				else	// aprovat
					//notes aprovades
					if (nota <= 10)
						min = nota < min? nota : min; //cuando una aprovada es menor que la anterior se cambian

			System.out.println("Introduce otra nota (numero negativo para acabar): ");
			nota = Double.parseDouble(System.console().readLine());
		}
		//a partir de aqui comprova si les notes son valides
		//dos notes no valides
		if (min == 11)	// no han hagut aprovats
			System.out.println("No has introduit notes aprovades o valides");
		else	// sí han hagut aprovats
			System.out.println("El aprobado más bajo es: " + min);
		if (max == -1)	// no han hagut suspesos
			System.out.println("No has introduit notes suspeses o valides");
		else	// sí han hagut aprovats
			System.out.println("El suspés més alt és " + max);

	}
}
