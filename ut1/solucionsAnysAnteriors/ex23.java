
import java.util.Scanner;

public class ex23{
    public static void main(String Args[]){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Enter:");
        int num = teclado.nextInt();
        int i=1;
        int prod=1;
    
        while(num<0){
            System.out.println("Valor positiu:");
            num = teclado.nextInt();
        }
        while(i<=num){
            prod *= i;	// prod = prod * i            
            i++;
        }
        System.out.println("El factorial de " + num + " és igual a " + prod);
    }
}
