//Exercisi 11. Programa que demana un nombre enter i imprimeix la seva taula de multiplicar.
//Per Diego Calatayud.

public class ex11
{

    public static void main (String args[])
    {
        int n1,n2;
        System.out.println("Introdueix un nombre enter:");
        n1 = Integer.parseInt(System.console().readLine());
        //System.out.println(" ");
        System.out.println("\nTAULA DEL " + n1 + ":");
        for (n2=1 ; n2<=10; n2++)
            System.out.println("   " + n1 + "x" + n2 + "=" + n2*n1);  
            
    }

}
