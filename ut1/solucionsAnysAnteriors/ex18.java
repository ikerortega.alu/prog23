// 18. Dissenyar un programa que mostre la suma dels nombres imparells compresos entre dos valors numèrics enters i positius introduïts per teclat. FERNANDO


public class ex18
{
    public static void main (String args[])
    {
        int n1, n2;
        do
        {
            System.out.print("Introdueix 2 valors positus: ");
            
 			n1 = Integer.parseInt(System.console().readLine());
            n2 = Integer.parseInt(System.console().readLine());

        }while((n1 < 0) || (n2 < 0));

        int suma = 0, aux;
        if(n1 > n2)
        {
            aux = n1;
            n1 = n2;
            n2 = aux;
        }
        if ((n1 % 2) == 0)	// si el n1 és parell..
        	n1++;	
        for(int i = n1; i <= n2; i+=2)
                suma += i;	
        System.out.println("La suma dels imparells és " + suma);  

        
    }


        
        
	
}
