//Algorisme que llija números des de teclat i finalitze al introduïr un major que la suma dels dos anteriors. Escriure en pantalla el nombre de valors introduïts i els valors que van complir la condició de finalització.

public class ex24
{
    public static void main (String args[])
    {
        double num1, num2, suma; int cont=2;

        System.out.println("2 números");
        num1 = Double.parseDouble(System.console().readLine());
        System.out.println("1 más");
        num2 = Double.parseDouble(System.console().readLine());

        System.out.println("El programa acabarà quan introduisques un número major a la suma dels 2 últims números");
                
        while(true)
        {
        	cont++;
            suma = num1 + num2;
            num1 = num2;
            System.out.println("Introduix altre valor:");
                
            num2 = Double.parseDouble(System.console().readLine());
            if (num2>suma)
            {
                System.out.println("La suma dels 2 últimos números es "+suma+"\nL'últim número ha sigut "+num2+" per tant s'ha acabat");
                break;
            }
            
        }
    }
}
