public class Operando
{
	private int n;	// entre 1 y 10

	public Operando()
	{
		n = 1 + (int)(Math.random()*10);
	}
	public int getValor() { return n; }
	public String toString() { return String.valueOf(n); }
}
