import java.util.*;

public class pruebaMates
{
	public static void main(String[] args)
	{
		Scanner ent = new Scanner(System.in);
		Operando opd1 = new Operando();
		Operando opd2 = new Operando();
		Operador opdr = new Operador();
		System.out.println("\t" + opd1 + opdr + opd2 + " = ?");	
		int resp = ent.nextInt(), soluc=0;
		switch (opdr.getValor())
		{
			case '+': soluc = opd1.getValor() + opd2.getValor(); break;
			case '-': soluc = opd1.getValor() - opd2.getValor();break;
			case '*': soluc = opd1.getValor() * opd2.getValor();break;
			case '/': soluc = opd1.getValor() / opd2.getValor();break;
		}
		if ( resp == soluc )
			System.out.println("BIEN, SIGUE ASÍ !!");
		else	
			System.out.println("INCORRECTO, REPASA LA OPERACIÓN ...");	
	}
}
