/* exemple de BufferedReader, ara llegirem línia a línia (més eficiència)
	Mostrarà també el seu propi codi font
*/
import java.io.*;
import java.util.*;

public class lecturaLiniaALinia
{
	public static void main(String[] args) {

		File f = new File("lecturaLiniaALinia.java");
		
			// UTILIZANT LA CLASSE JAVA.IO.BUFFEREDREADER
		if (f.exists())
		{
			try
			{
				FileReader fr = new FileReader(f);
				BufferedReader br = new BufferedReader(fr);
				// ara llegiré línia a línia mostrant cadascuna línia per pantalla
				String linia = br.readLine();
				while ( linia != null )	// null és el valor retornar per readLine() quan arribe al final del fitxer
				{
					System.out.println(linia);
					linia = br.readLine();
				}
				br.close();
				fr.close();
			}
			catch (IOException e)
			{
				System.out.println("S'ha generat l'IOException");
				System.err.println(e.getMessage());
			}

		}
		else // el fitxer no existeix
			System.out.println("El fitxer no existeix");	
			
		// UTILITZANT LA CLASSE JAVA.UTIL.SCANNER
/*		try
		{
			Scanner sc = new Scanner(f);
			if (f.exists())
			{
				String linia = sc.nextLine();
				while (true)
				{
					System.out.println(linia);
					linia = sc.nextLine();
				}
			}
			else
				System.out.println("El fitxer no existeix");
			sc.close();
		}
		catch (FileNotFoundException e)
		{
			System.err.println(e.getMessage());
		}
		catch (NoSuchElementException e)
		{
			//System.out.println("Final del fitxer");
	
		} */
		
	}
}
