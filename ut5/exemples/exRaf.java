/* codi que demane parells de valors numèrics (el primer sencer (posició) i el segon real (valor a escriure)) i escriga el valor en la posició
indicada. L'usuari escriurà tants parells com vullga, fins a acabar amb una posició igual a zero. */

import java.io.*;
import java.util.Scanner;

public class exRaf
{
	public static void main(String[] args) {
		int pos; double valor;
		Scanner ent = new Scanner(System.in);
		try (RandomAccessFile raf = new RandomAccessFile("nums.dat","rw"))
		{
			System.out.println("Introduix un parell de valors: posició (enter) i valor a escriure (real). Posició 0 per a acabar.");
			pos = ent.nextInt();
			while (pos != 0)
			{
				valor = ent.nextDouble();
				// ens posicionem on correspon segons el valor de posició ...
				raf.seek((pos-1)*Double.BYTES);
				// .. i ara ja podem escriure
				raf.writeDouble(valor);
				System.out.println("Introduix un parell de valors: posició (enter) i valor a escriure (real). Posició 0 per a acabar.");
				pos = ent.nextInt();
			}
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage());
		}
	}
}

