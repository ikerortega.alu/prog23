// Programa que mostra el seu propi codi font
import java.io.*;

public class lecturaByteAByte
{
	public static void main(String[] args) {
			File f = new File("lecturaByteAByte.java");
			if (f.exists())
			{
				try
				{
					FileInputStream fis = new FileInputStream(f);
					// Llegiré cada byte i el mostraré per pantalla
					char c;
					c=(char)fis.read();
					while ( c != (char)-1 )
					{
						System.out.print(c);	// mostre el caracter i continue llegint el següent
						c = (char)fis.read();
					}
					fis.close();
				}/*
				catch(FileNotFoundException e)
				{
					System.err.print("Excepció per no existir el fitxer");
				}*/
				catch(IOException e)
				{
					System.err.println(e.getMessage());
				}
			}
			else
				System.out.println("El fitxer no existeix");
			
		}	
}