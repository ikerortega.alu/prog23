// Exemple de DEseralització
import java.io.*;


public class deserialitzacio
{
	public static void main(String[] args) {
		File f = new File("usuaris.srz");
		if (f.exists())
		{
			try(FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);)
			// Deserialitze i mostre els objectes des de fitxer
/*			{
				Usuari u1 = (Usuari) ois.readObject();
				Usuari u2 = (Usuari) ois.readObject();
				Usuari u3 = (Usuari) ois.readObject();
				System.out.println(u1);
				System.out.println(u2);
				System.out.println(u3);
				
			}*/
			{
				while(true)
				{
					Usuari u= (Usuari) ois.readObject();
					System.out.println(u);
				}
			}
			catch(EOFException e)
			{
				//System.err.println(e.getMessage());
			}
			catch(ClassNotFoundException e)
			{
				System.err.println(e.getMessage());
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
		else 
			System.out.println("El fitxer no existeix");
	}
}
