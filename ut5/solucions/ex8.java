
/*8. Programa que, utilitzant la classe PushbackReader, faça substitucions sobre un fitxer de text de manera que, 
sempre que trobe una majúscula que no vaja precedida per un espai, inserisca el corresponent espai en blanc.

	Exemple:
	
		Hola.T'escric per a ...
		
			serà substituït per
			
		Hola. T'escric per a ...*/

import java.io.*;


public class ex8 {

    public static void main(String[] args) {

            File f= new File("alumnos.txt");
            File copia= new File("copia.txt");
        
       try (FileReader fr= new FileReader(f); BufferedReader br= new BufferedReader(fr); 
       FileWriter fw= new FileWriter(copia);
       ) {

            String s= br.readLine();

            while(s!=null)
            {
                StringReader sr = new StringReader (s); 
                PushbackReader pbr = new PushbackReader (sr);
                int ultimo = pbr.read (), penultimo = 0;

            while (ultimo!= -1)
            {
                if (ultimo=='.')
                {
                    ultimo=pbr.read();
                   
                
                    if (ultimo>=65 && ultimo<=90)	// SI ES MAJÚSCULA
                        fw.write(". "+(char)ultimo);
                    else
                    {
                        pbr.unread ((char) ultimo);
                        fw.write(".");
                    }
                } 
                else
                    fw.write((char) ultimo);
                    
                penultimo = ultimo;
                ultimo = pbr.read ();
            }
            fw.write("\n");
            s= br.readLine();
            
            }
            //f.delete();
            copia.renameTo(f);
 
       } 
       catch (IOException e) {
        System.err.println(e.getMessage());
       }
        
        
    }
}
