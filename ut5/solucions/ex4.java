//4. Realitza un programa que compte les línies, paraules i caràcters existents en un fitxer de text passat com a paràmetre (semblant al comandament "wc").
import java.io.*;

public class ex4
{
	public static void main(String[] args) 
	{
		// faltaria comprovar el pas de paràmetres a main
		try(BufferedReader br = new BufferedReader(new FileReader(args[0]));)
		{
			String s;
			int contCaracteres = 0, cont = 0, contPalabras = 0;
			while ((s = br.readLine()) != null)
			{
				contCaracteres += s.length();
				cont++;
				String palabras[] = s.split("\\s+");
				contPalabras += palabras.length;
			}
			System.out.println("Número de Líneas: "+cont+"\nNúmero de Caracteres: "+contCaracteres+"\nNúmero de palabras: "+contPalabras);
		}
		catch(IOException e){
			System.err.println(e.getMessage());
		}
	}
}
