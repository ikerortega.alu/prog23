/* 3. Crea un programa de gestió de discos de música. Per a això, defineix una classe Disc, sabent que per a cada disc volem registrar el títol (String), music o music (String), preu (per defecte, 15 euros però algun disc pot estar en oferta, rebaixant-lo un 20%). Afig a la classe els constructors i mètodes que consideres útils o necessaris.

 El programa ha de crear 3 discos, amb valors de títols i musics introduïts des de teclat per l'usuari. A més el programa posarà en oferta un únic disc, triat a l'atzar amb Math.random(). Finalment, mostrarà tots els discos amb els seus preus. */
 import java.util.Scanner;
 
 class Music
{
	private String nom;
	private String instrument;
	
	public Music()	// constructor per defecte
	{
		nom = "pepe";
		instrument = "piano";
	}
	
	public Music(String n,String i)	// constructor general
	{
		nom = n;
		instrument = i;
	}
	
	public Music(Music m)
	{
		nom = m.nom;
		instrument = m.instrument;
	}
	
	public void setNom(String n) { nom = n; }
	public void setInstrument(String i) { instrument = i; }
	public String getNom() { return nom; }
	public String getInstrument() { return instrument; }
	
	public void mostraMusic()
	{
		System.out.println("El músic " + nom + " toca l'instrument " + instrument);	
	}
	
	public String toString()
	{
		return  nom + " plays " + instrument;
	}
}

 class Disc{

	private String titol;
	private Music music;	// ara ja no és String, sino Music
	private double preu = 15;

	// CONSTRUCTOR PER DEFECTE
	public Disc(){
		titol = "Revolver";
		music= new Music();
		//preu = 15;
	}
	// constructor GENERAL
	public Disc(String t, Music m){
		titol = t;
		music = m;
	}
	
	public Disc(Disc d){
		titol = d.titol;
		music = d.music;
		//preu = d.preu;
	}

	public void setTitol(String t) { titol = t; }
	public void setMusic(Music m) { music = m; }
	public String getTitol() { return titol; }
	public Music getMusic() { return music; }
	public double getPreu() { return preu; }
	
	public double ofertaDisc(){
		preu = preu - preu * 0.2;	// preu = 0.8*preu;
		return preu;
	}
	 
	public void mostraDisc(){
		System.out.println("\nTítol: " + titol + "\nmusic o Music: " + music + "\nPreu: " + preu + "€");	// utilitza el toString de Music	
	}
	
	// toSring
	public String toString()
	{
		return "\nTítol: " + titol + "\nmusic o Music: " + music + "\nPreu: " + preu + "€";	// utilitza el toString de Music
	}
	 
}

 public class e4{
	
	public static void main(String[] args) {
		int oferta;

		oferta = (int)(3 * Math.random()) + 1;
		Disc d1 = new Disc(/*"Rumba", "Estepa"*/);	// constructor per defecte
		Music m1 = new Music("pepe","piano");
		Disc d2 = new Disc("USB", m1);	// constructor general
		Disc d3 = new Disc(d2);	// constructor de còpia
		
		switch(oferta){
			case 1:
				d1.ofertaDisc();break;
			case 2:
				d2.ofertaDisc();break;
			case 3:
				d3.ofertaDisc();break;
/*			default:
				System.out.println("Error");*/
		}
		d1.mostraDisc();
		d2.mostraDisc();
		//d3.mostraDisc();
		System.out.println(d3);	// crida al toString()
		// Canviar l'instrument del músic del disc d3
		d3.getMusic().setInstrument("guitar");
		System.out.println(d3);
		
		
		
	}
}
