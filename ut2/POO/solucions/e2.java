/* 2. Crea una classe Music que incloga 2 atributs (nom del músic i instrument, tots dos de tipus String). La classe inclourà també, almenys:

 - un constructor que inicialitze el músic amb un nom i un instrument inicial.
 - constructor de còpia.
 - un mètode mostraMusic (o toString()) que mostre les dades del músic per pantalla.
 - els corresponents mètodes "setters" i "getters" (setNom,setInst,getNom,getInstr) que canvien l'instrument o el nom (mètodes setXxx), o retornen aqueixos mateixos valors (mètodes getXxx).

	Realitza un programa que, utilitzant la classe Musica, definisca almenys dos objectes d'aquest tipus, mostre les seues dades, modifique alguna dada de cadascun d'ells i acabe mostrant els seus noms, instruments i el total de músics. */
	
class Music
{
	private String nom;
	private String instrument;
	
	public Music()	// constructor per defecte
	{
		nom = "pepe";
		instrument = "piano";
	}
	
	public Music(String n,String i)	// constructor general
	{
		nom = n;
		instrument = i;
	}
	
	public Music(Music m)
	{
		nom = m.nom;
		instrument = m.instrument;
	}
	
	public void setNom(String n) { nom = n; }
	public void setInstrument(String i) { instrument = i; }
	public String getNom() { return nom; }
	public String getInstrument() { return instrument; }
	
	public void mostraMusic()
	{
		System.out.println("El músic " + nom + " toca l'instrument " + instrument);	
	}
	
	public String toString()
	{
		return  nom + " plays " + instrument;
	}
}

public class e2
{
	public static void main(String args[])
	{
		Music m1 = new Music();	// constructor per defecte
		Music m2 = new Music("John Coltrane","saxofó");	// constructor general
		Music m3 = new Music(m1);	// constructor de còpia
		
		m1.mostraMusic();
		m2.mostraMusic();
		m3.mostraMusic();
		
		m1.setNom("Jose");
		m2.setInstrument("saxo");
		m3.setInstrument("teclat electrònic");
		
		System.out.println(m1);		// equivalent a System.out.println(m1.toString());	
		System.out.println(m2);
		System.out.println(m3);
		
	}
	
}	
	
