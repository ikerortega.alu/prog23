/*5. Defineix la classe Dau amb:

	- un atribut "valor" (que variarà entre 1 i 6)
	- constructor sense paràmetre
	- constructor general amb valor indicat com a paràmetre
	- constructor de còpia
	- setter i getter (setValor i getValor)
	- un mètode "tirada" que canvia el valor del dau a l'atzar
	- altre mètode "mostra" que dibuixe el dau en pantalla amb asteriscos
		
Realitza a continuació un programa que instàncie al almenys 2 objectes Dau, cadascun amb un constructor diferent, i compte quantes tirades s'han de fer per a generar 3 "6" consecutius.*/
//import java.util.Scanner;
public class dado 
{
	//propiedades o atributos de la clase
	private int valor;
	//private int aleatorio;
	
	
	//metodos (funciones internas) de la clase
	//CONSTRUCTORES
	//constructor por defecto
	public dado()
	{
		valor=1;
	}
	//constructor general
	public dado(int va)
	{
		valor=va;
			
	}
	//constructor de copia
	public dado (dado d)
	{
		valor=d.valor; //se asigna el valor de ancho y alto, que tiene r al nuevo objeto
	}
	//primero los metodos "SETTERS"
	public void setValor (int va)
	{
		valor=va;
	}
	//a continuacion los metodos "GETTERS"
	public int getValor()
	{
		return valor;
	}	
	//otros metodos: que me interesen para la abstraccion.
	//es la idea que necesitas de cada clase
	public void tirada()
	{
		valor = (int)(6*Math.random())+1;
	}
	public void muestraDado() 
	{
		System.out.println("valor del dado : "+valor);
	}
	
	public void muestra()
	{	
		switch(valor)
		{
		case 1:
			System.out.println("     ");
			System.out.println("     ");
			System.out.println("  *  ");
			System.out.println("     ");
			System.out.println("     ");break;
		case 2:
			System.out.println("      ");
			System.out.println("    * ");
			System.out.println("       ");
			System.out.println(" *   ");
			System.out.println("      ");break;
		case 3:
			System.out.println("     *");
			System.out.println("      ");
			System.out.println("   *  ");
			System.out.println("      ");
			System.out.println(" *    ");break;
		case 4:
			System.out.println(" *   *");
			System.out.println("      ");
			System.out.println("      ");
			System.out.println("      ");
			System.out.println(" *   *");break;
		case 5:
			System.out.println(" *   *");
			System.out.println("      ");
			System.out.println("   *  ");
			System.out.println("      ");
			System.out.println(" *   *");break;
		case 6:
			System.out.println(" *   *");
			System.out.println("      ");
			System.out.println(" *   *");
			System.out.println("      ");
			System.out.println(" *   *");
		}
	}
	
	public int genera666()
	{  int cont2=0,cont=0;
		while(cont<3)
		{  
			// tire el dau
			tirada();
			if (valor==6)
				cont++;
			else
				cont=0;
			cont2++;
		}
		//System.out.println("Han salido 3 6 consecutivos despues de "+cont2+" tiradas.");
		return cont2;
	}
	
}

