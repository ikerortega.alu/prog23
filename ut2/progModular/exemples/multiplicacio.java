/* 
    Programa que demana 2 valors numèrics i mostra el seu producte */

public class multiplicacio
{
    // FUNCIÓ QUE OBTINGA EL RESULTAT DEL PRODUCTE A PARTIR DE 2 VALORS
    /* num1 i num2 SON PARÀMETRES FORMALS */
    public static double producte(double num1,double num2)
    {
    	return num1*num2;
 
    }
    public static void main(String args[])
    {
        double n1,n2/*,result*/;

        System.out.println("Introdueix 2 valors numèrics:");
        n1 = Double.parseDouble(System.console().readLine());
        n2 = Double.parseDouble(System.console().readLine());
        //result = n1 * n2;
        //System.out.println("El producte és " + n1 * n2);
        
        // ÚS DE LA FUNCIÓ PRODUCTE
        /*result = producte(n1,n2);
        System.out.println("El producte és " + result);*/
        /* n1 i n2 SON PARÀMETRES ACTUALS */
        System.out.println("El producte és " + producte(n1,n2));	
        // el valor de n1 se li assgina a num1 i el valor de n2 se li assigna a num2
        
    }
    

}

