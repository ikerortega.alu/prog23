// Exemple de funció que SI retorna un valor (no és, per tant, void)

// Programa modular que demane 2 números i mostre la mitjana aritmètica

public class funcioNOVoid
{
	public static void main(String[] args) {
		double num1,num2/*,result*/;
		// demanar 2 números a l'usuari
		System.out.println("Introduix 2 números:");
		num1 = Double.parseDouble(System.console().readLine());
		num2 = Double.parseDouble(System.console().readLine());		
		// ús de la funció
		//result = mitjana(num1,num2);
		// mostrem el resultat per pantalla
		//System.out.println("La mitjana de " + num1 + " i " + num2 + " és igual a " + result);
		System.out.println("La mitjana de " + num1 + " i " + num2 + " és igual a " + mitjana(num1,num2));
		System.exit(0);
	}
	
	// Definició de la funció
	public static double mitjana(double n1,double n2)
	{
		return (n1+n2)/2;
	}
}
