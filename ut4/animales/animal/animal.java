package animal;

public class animal
{
	protected double peso;
	protected String color;
	
	public animal()
	{
		peso=300;
		color="Esmeralda";
	}
	
	public animal(double peso,String color)
	{
		this.peso=peso;
		this.color=color;
	}
	
	public String getColor()
	{
		return color;
	}
	
	public double getPeso()
	{
		return peso;
	}
	
	public void setColor(String c)
	{
		color=c;
	}
	
	public void setPeso(double p)
	{
		peso=p;
	}
	
	public String toString()
	{
		return "Peso= "+peso+" color= "+color;
	}
}
