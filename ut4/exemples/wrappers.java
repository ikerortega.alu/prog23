class wrappers {
   public static void main(String[] args) {
	//Integer i1 = new Integer(5); 
	Integer i1 = Integer.valueOf(5);
	//Integer i2 = new Integer("7"); 
	Integer i2 = Integer.valueOf("7");
	String s1 = i1.toString();
	System.out.println(s1);//mostra 5 per pantalla
	int i3 = Integer.parseInt("10",10);
	int i4 = Integer.parseInt("10",8);
	int i5 = Integer.parseInt("BABA",16);
	System.out.println(i3);//mostra 10 per pantalla
	System.out.println(i4);//mostra 8 per pantalla
	System.out.println(i5);//mostra 47802 per pantalla
	System.out.println(Integer.toOctalString(i4));//mostra 10 per pantalla
	System.out.println(Integer.toHexString(i5));//mostra baba per pantalla
	int i6 = Integer.valueOf("22").intValue();
	System.out.println(i6);//mostra 22 per pantalla
    }
}
