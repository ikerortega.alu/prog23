// Classe DERIVADA
package figures;

public class Rectangle extends Figura
{
	// propietats o atributs de la classe
	private double ample;
	private double alt;
	
	// mètodes (funcions internes) de la classe
	// CONSTRUCTORS
	// constructor per defecte
	public Rectangle()
	{
		super();	// crida al constructor de la classe base: Figura per a possar color negre
		alt=1;ample=2;	// alt=ample=1
	}
	// constructor general
	public Rectangle(String c,double am, double al)
	{
		super(c);	// crida al constructor de la classe base: Figura per a possar color c
		ample = am; // atribut = paràmetre
		alt = al;
	}
	// constructor alternatiu
	public Rectangle(double am)
	{
		ample = am;
		alt = 1;
	}
	// CONSTRUCTOR DE CÒPIA
	public Rectangle(Rectangle r)
	{
		super(r.color);
		ample = r.ample;	// assigne els valor d'ample i alt que té r al nou objecte
		alt = r.alt;
	}
	
	// primer els mètodes "SETTERS"
	public void setAmple(double amp)
	{
		ample = amp;
	}
	public void setAlt(double al)
	{
		alt = al;	// atribut = paràmetre
	}	
	// a continuació els mètodes GETTERS
	public double getAmple()
	{
		return ample;
	}
	public double getAlt()
	{
		return alt;
	}
	// altres mètodes: els que m'interessen per a la meua abstracció
	public double area()
	{
		return ample*alt;
	}
	public double perimetre()
	{
		return 2*(ample+alt);
	}
	public void mostraRectangle()
	{
		System.out.println("Ample: " + ample + " , alt: " + alt + ", color: " + color);
	
	}
	@Override
	public String toString()
	{
		return "Ample: " + ample + " , alt: " + alt + ", color: " + color;
	}

}
