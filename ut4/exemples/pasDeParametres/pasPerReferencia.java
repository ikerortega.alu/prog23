// Programa que demane un valor a l'usuari i mostre el seu valor duplicat

/* 
  ARA PASSARÉ EL VALOR PER REFERÈNCIA: EL VALOR ESTÀ INTERN A L'ARRAY. EL NOM DE L'ARRAY ÉS LA REFERÈNCIA */

import java.util.Scanner;
public class pasPerReferencia
{
	private static Scanner ent = new Scanner(System.in);
	
	public static void main(String args[])
	{
		double num[] = new double[1];
		
		System.out.println("Introduix un número:");
		num[0] = ent.nextDouble();
		duplica(num);
		System.out.println("El valor duplicat és " + num[0]);

	}
	
	public static void duplica(double n[])
	{
		n[0] = 2*n[0];
		//System.out.println("Dintre de la funció: el valor duplicat és " + n);
	}
}
