public class cercle
{
	public double radi;
	
	public cercle() { radi=1;}
	public cercle(double r) { radi = r; }
	
	public void setradi(double r) { radi=r; }
	public double getradi() { return radi; }
	
	public String toString() { return " radi: " + radi; }
}
