// En aquest programa clonem un objecte dibuix per a comprovar si original i copia són, o no, completament independents

public class programa
{
	public static void main(String[] args) {
		dibuix d1 = new dibuix();	// num=1, rectángulo i círculo unitaris
		dibuix d2 = /*(dibuix)*/ d1.clone();
		dibuix d3 = new dibuix(d1);	// utilizant el Constructor de còpia
		System.out.println("Mostrem els 3 dibuixos: han de ser iguals");
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);
		System.out.println("Ara canviaré el número del segon dibuix, no afectarà ni al primer ni al segon");
		d2.setNum(2);
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);
		// canvie l'ample del rectangle en el primer dibuix a 10
		d1.getrectangle().setample(10);
		//d1.setample(10);
		d3.getcercle().setradi(8);
		System.out.println("Després de fer els canvis");
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);	
		System.out.println("Observa si el canvi en el primer dibuix o en el tercer ha afectact al altres dibuixos...");
		
		
		System.exit(0);
	}
}
