// Donat que vull clonar objectes dibuix, la classe ha d'implementar la interfície Cloneable
public class dibuix implements Cloneable
{
	private int num;
	private rectangle r;
	private cercle c;
	
	public dibuix() { num=1; r = new rectangle(); c = new cercle(); }
	public dibuix(dibuix d)	// Constructor de COPIA
	{
		num = d.num;
		/*r = d.r;
		c = d.c;*/
		r = new rectangle(d.getrectangle().getample(),d.getrectangle().getalt());
		c = new cercle(d.getcercle().getradi());
	}
	public void setNum(int n) { num = n; }
	public String toString() { return "num: " + num + ", " + r.toString() + c.toString(); }
	public rectangle getrectangle() { return r; }
	public void setrectangle(rectangle rect) { r = rect; }
	public cercle getcercle() { return c; }
	public void setcercle(cercle circ) { c = circ; }
	//public void setample(double an) { r.setample(an); }
	// sobreescribo el método clone() heredado de Object
	@Override
	public dibuix clone()
	{
		dibuix o = null;
		try
		{
			o = (dibuix)super.clone();
			//o.setrectangle(r);
			o.setrectangle(new rectangle(r.getample(),r.getalt()));
			//o.setcercle(c);
			o.setcercle(new cercle(c.getradi()));
		}
		catch(CloneNotSupportedException e)
		{
			System.err.println(e.getMessage());
		}
		return o;
	}
}
