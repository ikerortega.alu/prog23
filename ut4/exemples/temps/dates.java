import java.util.Date;

class dates
{
	public static void main(String[] args) {
		// es crea un objecte de Date amb els milisengons transcorreguts des del primer instant de 1970
		Date d=new Date();
		System.out.println("Són les "+d.toString());
		System.out.println("Han passat "+d.getTime()+" milisegons desde l'ERA");
		System.out.println("Convertint a 2023...");
		d.setYear(123);	// 1900 + 123
		System.out.println("Són les "+d.toString());

		System.exit(0);
	}
}
