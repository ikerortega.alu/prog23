// EXEMPLE D'INTERFÍCIE FUNCIONAL SENSE LAMBDA AMB CLASSE ANÒNIMA

// Exemple que carrega de Strings un objecte Vector i els mostra per pantalla
import java.util.*;
import java.util.function.Consumer;

public class consumer3
{
	public static void main(String args[])
	{
		Vector<String> vector = new Vector<String>();
		
		vector.add("Sergio");
		vector.add("Daniel");
		vector.add("Roberto");
		vector.add("Jorge");
		System.out.println("El contingut del objecte Vector és:");
		// 1. Amb bucle foreach
		for(String s: vector)
			System.out.println(s);
		System.out.println("El contingut del objecte Vector és:");
		// 2. Amb iterator
		Iterator<String> it = vector.iterator();
		while (it.hasNext())
			System.out.println(it.next());
		System.out.println("El contingut del objecte Vector és:");
		/* 3. Amb el mètode foreach() que usa una interfície funcional (resolta SENSE expressió lambda)
			FARÉ UNA CLASSE ANÓNIMA QUE IMPLEMENTE LA INTERFÍCIE FUNCIONAL */
		Consumer<String> objecteConsumer = new Consumer<String>()
		{	// per a definir el cos de la classe anònima
			public void accept​(String s)
			{
				System.out.println(s);
			}
		};
		vector.forEach(objecteConsumer);
	}
}
/*
/* sense lambda estic obligat a crear una classe que implemente (implements) l'interfície 
class Consumidora implements Consumer<String>
{
	// és una interfície funcional: només he d'implementar el mètode accept()
	public void accept​(String s)
	{
		System.out.println(s);
		
	}
}*/
