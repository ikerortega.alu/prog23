import ocells.Ocell;
import ocells.Colibri;

public class pajareria{
	public static void main(String[] args) throws CloneNotSupportedException {
		
		Colibri c1= new Colibri (1.2, "verde");
		System.out.println("Colibri 1: ");
		c1.pia();
		c1.vola();
		c1.cova();
		System.out.println(c1);

		Colibri c2 = (Colibri) c1.clone();
		//System.out.println();
		System.out.println("\nColibri 2 copiado:");
		c2.setColor("amarillo");
		c2.setLogitudBec (2.6);
		c2.pia();
		c2.vola();
		c2.cova();
		System.out.println(c2);
	}
}


/*
¿Què canviaria si els objectes colibrí es crearen amb una referència a Ocell? Explica-ho en el propi
codi font del programa.

No podriem accedir, per exemple, a setColor i en general no podríem accedir als membres de la classe derivada. Sí podría fent el "casting"
*/
