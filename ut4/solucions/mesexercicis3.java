/*3. Crear objeto gregorian calendar y que muestre los seis valores 
(dias mes años horas minutos y segundos)en los que se ha creado ese objeto.*/

import java.util.*;

public class mesexercicis3
{
	public static void main(String[] args) {
		//Date d=new Date();
		GregorianCalendar gc =new GregorianCalendar();
		//gc.setTime(d);
		System.out.print(gc.get(Calendar.DAY_OF_MONTH));
		System.out.print("-");
		System.out.print(gc.get(Calendar.MONTH)+1);
		System.out.print("-");
		System.out.println(gc.get(Calendar.YEAR));
		System.out.println("Horas: "+gc.get(Calendar.HOUR_OF_DAY));
		
		System.out.println("Minutos: "+gc.get(Calendar.MINUTE));
		
		System.out.println("Segundos: "+gc.get(Calendar.SECOND));

	}
}
