package viaje;

public class vehiculo implements Transport
{
    protected double potencia;
    protected double consumo;

    public vehiculo(double consumo, double potencia)
    {
        if(potencia<0)
            this.potencia=potencia*-1;
        /*else
        this.potencia=potencia;*/

        if(consumo<0)
            this.consumo=consumo*-1;
        /*else
        this.consumo=consumo;*/
    }

    public void setPotencia(double potencia)
    {
        this.potencia=potencia;
    }
    public void setConsumo(double consumo)
    {
        this.consumo=consumo;
    }

    public double getPotencia()
    {
        return potencia;
    }
    public double getConsumo()
    {
        return consumo;
    }

    public String toString()
    {
        return "El vehiculo tiene una potencia de "+potencia+" y un consumo de "+consumo+" litros por Km";
    }

    public double consumViatge(double km)
    {
        //por cada km se cobran 20€
        return km*20;
    }
}
