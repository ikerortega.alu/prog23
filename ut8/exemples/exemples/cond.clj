; Programa que, donat el dia de la setmana (enter), indique si és dia laboral o cap de setmana
(println "Introduix dia de la setmana (entre 1 i 7):")
(def dia (Integer/parseInt (read-line)))
(cond
	(< dia 6) (println "dia laboral")
	(>= dia 6) (println "cap de setmana")
	:else (println "dia incorrecte")
)
