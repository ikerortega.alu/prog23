; Programa que demane un dia de la setmana (entre 1 i 7) y mostre el seu nom textual

(println "Introduix el dia de la setmana (entre 1 i 7):")
(def dia (Integer/parseInt (read-line)))
(case dia
	1 (println "dilluns")
	2 (println "dimarts")
	3 (println "dimecres")
	4 (println "dijous")
	5 (println "divendres")
	6 (println "dissabte")
	7 (println "diumenge")
	(println "Dia incorrecte")
)
