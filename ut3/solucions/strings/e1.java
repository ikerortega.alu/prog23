//1. Fer un programa que ingresse una cadena de caràcters i determine el nombre de majúscules i el nombre de minúscules.
import java.util.Scanner;
public class e1 {
  public static void main(String[] args) {
    //String cadena = "Entornos de desarrollo";
    Scanner ent = new Scanner(System.in);
    System.out.println("Introduix una frase:");
    String cadena = ent.nextLine();
    
    int mayusculas = 0,minusculas = 0;

    // Recorremos cada carácter de la cadena
    for (int i = 0; i < cadena.length(); i++) {
      char c = cadena.charAt(i);

      // Si el carácter es una letra mayúscula, aumentamos el contador
      if (Character.isUpperCase(c)) 
             mayusculas++;
      // Si el carácter es una letra minúscula, aumentamos el contador
      else
       if (Character.isLowerCase(c)) 
              minusculas++;
    }

    // Mostramos el resultado
    System.out.println("Hay " + mayusculas + " letras mayúsculas en la frase.");
    System.out.println("Hay " + minusculas + " letras minúsculas en la frase.");
  }
}

