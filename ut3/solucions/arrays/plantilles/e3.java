public class e3
{
	public static void main(String args[])
	{
		int nums[] = new int[100];
		
		carregaValors(nums);
		mostraValors(nums);
		System.out.println("Introduix un valor entre 1 i 100:");
		valor = ...;
		if ( cercaValor(valor,nums) )
			... // indique si es troba, o no, en l'array
		else
			...
		ordenaValors(nums);
		mostraValors(nums);
		...
	}
	
	... carregaValors(int nums[])
	{
		// Math.random() entre 1 i 100, dintre d'un for
	}
	
	.... mostraValors(int nums[])
	{
		// mostrar el contingut de l'array
	}
	
	... cercaValor(v, int nums[])
	{
		// algorisme de recerca linial booleana
	}
	
	.... ordenaValors(int nums[])
	{
		// mètode de la bambolla
		
	}
}
