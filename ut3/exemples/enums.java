// GENERA TOTA LA BARALLA I, AL FINAL, UNA MÉS ALEATÒRIA

import java.util.*;

enum Rang
{ DOS, TRES, QUATRE, CINC, SIS, SET, SOTA, CABALL, REI, AS }

enum Familia
{ BASTOS, ESPASES, ORS, COPES }

// classe que representa una carta de la baralla
class Carta
{
	private final Rang rang;
	private final Familia familia;

/* You can't see this method values() in javadoc because it's added by the compiler.

Enum Types, from The Java Tutorials
The compiler automatically adds some special methods when it creates an enum. For example, they have a static values method that returns an array containing all of the values of the enum in the order they are declared. This method is commonly used in combination with the for-each construct to iterate over the values of an enum type. */

	public Carta()
	{
		this.rang = Rang.values()[(int) (Math.random()* (Rang.AS.ordinal()+1))];
		this.familia = Familia.values()[(int) (Math.random()* (Familia.COPES.ordinal()+1))];
	}
	public Carta(Rang rang, Familia familia)
	{
		this.rang = rang;
		this.familia = familia;
	}

	public Rang getRang() { return rang; }
	public Familia getFamilia() { return familia; }
	public String toString() { return rang + " DE " + familia; }
}

public class enums
{
	private static final Vector<Carta> v = new Vector<Carta>();
	
	// inicilitzador static: CONSTRUEIX LA BARALLA AMB LES 40 (4*10) CARTES
	static
	{
		for (Familia familia : Familia.values())	// bucle foreach
			for (Rang rang : Rang.values())	// bucle foreach
				v.add(new Carta(rang, familia));
	}
	
	public static void main(String[] args)
	{
		System.out.println("Les cartes de la baralla són: ");
		
		
		Iterator it = v.iterator();
		while(it.hasNext())
			System.out.println(it.next());
			
		System.out.println("Anem a triar una carta aleatoriament");
		
			
		//Carta c = new Carta(Rang.QUATRE,Familia.ESPASES);
		Carta c = new Carta();
		System.out.println(c);
		
		System.exit(0);
	}
}
