import java.util.StringTokenizer;

public class exStringTokenizer
{
	public static void main(String args[])
	{
		StringTokenizer st = new StringTokenizer ( "La música calma a les feres","ú");
		//StringTokenizer st = new StringTokenizer ( "La música calma a les feres", "ae");
		// obtinc els tokens "L" " músic" " c" "lm" " l" "s f" "r" "s"
		int num = st.countTokens();
		System.out.println ( "El text original ha quedat dividit en " + num + " elements.");
		// mostrem cadascun dels elements
		//for (int i = 0; i <num; i ++)
		while (st.hasMoreTokens())
			System.out.println (st.nextToken ());
	}
}
