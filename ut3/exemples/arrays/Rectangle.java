// Primera classe INSTANCIABLE

public class Rectangle
{
	// propietats o atributs de la classe
	private double ample;
	private double alt;
	
	// mètodes (funcions internes) de la classe
	// CONSTRUCTORS
	// constructor per defecte
	public Rectangle()
	{
		alt=1;ample=2;	// alt=ample=1
	}
	// constructor general
	public Rectangle(double am, double al)
	{
		ample = am; // atribut = paràmetre
		alt = al;
	}
	// constructor alternatiu
	public Rectangle(double am)
	{
		ample = am;
		alt = 1;
	}
	// CONSTRUCTOR DE CÒPIA
	public Rectangle(Rectangle r)
	{
		ample = r.ample;	// assigne els valor d'ample i alt que té r al nou objecte
		alt = r.alt;
	}
	
	// primer els mètodes "SETTERS"
	public void setAmple(double amp)
	{
		ample = amp;
	}
	public void setAlt(double al)
	{
		alt = al;	// atribut = paràmetre
	}	
	// a continuació els mètodes GETTERS
	public double getAmple()
	{
		return ample;
	}
	public double getAlt()
	{
		return alt;
	}
	// altres mètodes: els que m'interessen per a la meua abstracció
	public double area()
	{
		return ample*alt;
	}
	public double perimetre()
	{
		return 2*(ample+alt);
	}
	public void mostraRectangle()
	{
		System.out.println("Ample: " + ample + " , alt: " + alt);
	
	}

}
